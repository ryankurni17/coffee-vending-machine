#include "HX711.h"
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <Servo.h>

#define Mdc_Kopi  6
#define Mdc_Gula  7 

LiquidCrystal_I2C lcd(0x27,20,4);   // Library untuk Konfigurasi LCD menggunakan I2C
HX711 scale;                        // Library LoadCell Konfigurasi Membaca Beban 

const int LOADCELL_DOUT_PIN = A1;   // inisiasi Seluruh pin Yang digunakan pada rangkaian
const int LOADCELL_SCK_PIN = A0;  
const int T_Merah = 3;
const int T_Hijau = 4;
const int T_Biru = 5;
const int PompaAir = 9; 
const int ServoLC = 10;

Servo servo;              // inisiasi nama data Servo

int angle = 95;           // inisiasi sudut servo awal 95 derajat
int lastButtonState;      // inisiasi Tombol pertama ketika Ditekan
int currentButtonState;   //  inisiasi Tombol Kedua ketika Ditekan

void setup() {
  Serial.begin(9600);     // pembacaan data melalui serial monitor 
  lcd.init();             // inisiasi LCD
  lcd.backlight();        // inisiasi cahaya LCD
  
  pinMode(T_Biru, INPUT);   // proses mengimput data Tombol
  pinMode(T_Hijau, INPUT);
  pinMode(T_Merah, INPUT);
  pinMode(Mdc_Kopi, OUTPUT);     // Proses Pemanggilan Relay 3
  pinMode(Mdc_Gula, OUTPUT);     // Proses Pemanggilan Relay 1
  pinMode(PompaAir, OUTPUT);     // Proses Pemanggilan Relay 4

  digitalWrite(Mdc_Kopi, HIGH);   // Proses Mematikan Relay 3
  digitalWrite(Mdc_Gula, HIGH);   // Proses Mematikan Relay 1
  digitalWrite(PompaAir, HIGH);   // Proses Mematikan Relay 4

  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);   // inisiasi library pembacaan sensor loadcell
  scale.set_scale(440.0);                             // Kalibrasi Sensor Loadcell awal pembacaan berat yang digunakan
  scale.tare(5);                                      // Kalibrasi Berat sensor LoadCell
  
  servo.attach(ServoLC);          
  servo.write(angle);
  currentButtonState = digitalRead(T_Biru);

  lcd.setCursor(2,0);                                  // (0,0) bagian angka kiri untuk membuat kolom dan bagian kanan angka kanan yuntuk membuat baris
  lcd.print("'Coffee Vending'");                       // menampikan Tulisan Pada LCD Display 
  lcd.setCursor(1,1);
  lcd.print("Machine Dispensers");
  lcd.setCursor(3,2);
  lcd.print("^ Production ^");
  lcd.setCursor(2,3);
  lcd.print("^ BY 3 Sister' ^");
  delay(5000);                                         // Waktu Tunda 5 detik
  lcd.clear();                                         // Untuk menghapus Tulisan pada LCD Display
}

void loop() {
  lcd.setCursor(3,1);
  lcd.print("Coffee Vending");
  lcd.setCursor(1,2);
  lcd.print("Machine Dispensers");
 
  Konfirmasi();         // Parameter Mengambil Data Konfirmasi
  ProsesPompaAir();     // Parameter Mengambil Data Proses Penuangan Air
  ProsesKopi_Gula();    // Parameter Mengambil Data Proses pengisian Kopi,Gula Beserta Melakukan Proses Menimbang Berat

  scale.power_down();   // untuk Membaca Berat Ketika Posisi Kosong Tanpa Beban
  scale.power_up();     // Untuk Membaca Berat Ketika Posisi Terdapat Beban
}

void Konfirmasi() {                             // Data Parameter Program Proses kopi saat Tombol Biru ditekan
  lastButtonState    = currentButtonState;      // Kondisi Untuk Menyimpan Data Tombol Yg Terakhir di Tekan
  currentButtonState = digitalRead(T_Biru);     // Kondisi Ketika Tombol Baru Di Tekan

  if(lastButtonState == HIGH && currentButtonState == LOW) {    // Pengondisian Tombol Ketika Di Tekan
    Serial.println("The button is pressed");

    // change angle of servo motor
    if(angle == 95){      // kondisi Ketika Tombol Biru Di Tekan pertama maka menggerakan servo ke posisi 0
      angle = 0;
      lcd.clear();
      lcd.setCursor(0,1);
      lcd.print("Proses Telah Selesai");
      lcd.setCursor(1,2);
      lcd.print("Selamat Menikmati!");
      delay(2000);
      lcd.clear();
    }
    else{
    if(angle == 0)        // kondisi Ketika Tombol Biru Di Tekan pertama maka menggerakan servo ke posisi 95
      angle = 95;
      lcd.clear();
    }
    servo.write(angle);   // Proses menjalankan sudut servo
  }
}

void ProsesKopi_Gula() {                    // Data Parameter Program Proses kopi saat Tombol Merah ditekan
  int Kondisi_TM = digitalRead(T_Merah);

  if (Kondisi_TM == HIGH) {
    Serial.println("The button is being pressed");
    digitalWrite(Mdc_Kopi, LOW);
    lcd.clear();
    lcd.setCursor(3,1);
    lcd.print("Mohon Tunggu !");
    lcd.setCursor(0,2);
    lcd.print("Proses Pemuatan Kopi");
    delay(3000);
    digitalWrite(Mdc_Kopi, HIGH);
    digitalWrite(Mdc_Gula, LOW);
    lcd.setCursor(3,1);
    lcd.print("Mohon Tunggu !");
    lcd.setCursor(0,2);
    lcd.print("Proses Pemuatan Gula");
    delay(3000);
    digitalWrite(Mdc_Gula, HIGH);
    timbangan();
    delay(5000);
    lcd.clear();
  }
  else
    digitalWrite(Mdc_Kopi, HIGH);
    digitalWrite(Mdc_Gula, HIGH);
}

void ProsesPompaAir() {                     // Data Parameter Program Proses kopi saat Tombol Hijau ditekan
  int Kondisi_TH = digitalRead(T_Hijau);
  
  if (Kondisi_TH == HIGH) {
    lcd.clear();
    lcd.setCursor(3,1);
    lcd.print("Mohon Tunggu !");
    lcd.setCursor(0,2);
    lcd.print("Proses Pengisian Air");
    Serial.println("The button is being pressed");
    digitalWrite(PompaAir, LOW);
    delay(3000);
    lcd.clear();
  }
  else
    digitalWrite(PompaAir, HIGH);
}

void timbangan(){                   // Data Parameter Program Proses Menimbang berat Dari kopi dan gula
  Serial.print("Berat");
  float berat= scale.get_units(5);
  if(berat<=0.1)
  {
    berat=0.0;
  }
  Serial.println(berat,1);
  if(berat<1000)
  {
  lcd.clear();
  lcd.setCursor(2,1);
  lcd.print("Proses Menimbang");
  lcd.setCursor(4,2);
  lcd.print("Berat = ");
  lcd.print(berat,1);
  lcd.print(" g");
  }
  if(berat>=1000)
  {
  lcd.clear();
  lcd.setCursor(2,1);
  lcd.print("Proses Menimbang");
  float hasil=berat/1000;
  lcd.setCursor(4,2);
  lcd.print("Berat = ");
  lcd.print(hasil);
  lcd.print(" kg");
  }
}
