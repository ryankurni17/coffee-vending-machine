#include <Servo.h>
Servo myservo; 

void setup() {
  // put your setup code here, to run once:
  myservo.attach(10);
  myservo.write(95);
}

void loop() {
   myservo.write(95);
   delay(5000);
   myservo.write(130);
   delay(5000);
   myservo.write(60);
   delay(5000);
   myservo.write(95);
   delay(5000);
   myservo.write(0);
   delay(5000);
}
