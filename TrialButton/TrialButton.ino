const int BUTTON_PIN = 4;  // Connect the Button to pin 7 or change here

// variables will change:
int State = LOW;        // tracks the current state of LED
int lastButtonState;        // the previous state of button
int currentButtonState; // the current state of button
  
void setup() {
  Serial.begin(9600);                         // initialize serial
  pinMode(BUTTON_PIN, INPUT);  // set arduino pin to input mode
  
  currentButtonState = digitalRead(BUTTON_PIN);
}
  
void loop() {
  lastButtonState    = currentButtonState;                // save the last state
  currentButtonState = digitalRead(BUTTON_PIN); // read new state
  
  if(lastButtonState == HIGH && currentButtonState == LOW) {
    Serial.print("The button is pressed: ");
  
    // toggle state of LED
    if(State == LOW) {
       State = HIGH;  
       Serial.println("Turning LED on");
    }
    else {
      State = LOW;
      Serial.println("Turning LED off");
    }
 
    // control LED arccoding to the toggled state
    //digitalWrite(LED_PIN, State);  //turns the LED on or off based on the variable
  }
}
