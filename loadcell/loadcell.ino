#include "HX711.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// HX711 circuit wiring
const int LOADCELL_DOUT_PIN = A1;
const int LOADCELL_SCK_PIN = A0;  

LiquidCrystal_I2C lcd(0x27,20,4);
HX711 scale;

void setup() {
  Serial.begin(9600);
  Serial.println("Pastikan Tidak Ada Beban");
   lcd.init();
  lcd.backlight();
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  scale.set_scale(440.0);                      // this value is obtained by calibrating the scale with known weights; see the README for details
  scale.tare(5);				        // reset the scale to 0
  Serial.println("Silahkan Untuk Menimbang");
}

void loop() {
  timbang();
}

void timbang(){
  Serial.print("Berat");
  float berat= scale.get_units(5);
  if(berat<=0.1)
  {
    berat=0.0;
  }
  Serial.println(berat,1);
  if(berat<1000)
  {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("SILAHKAN TIMBANG");
  lcd.setCursor(0, 1);
  lcd.print("Berat= ");
  lcd.print(berat,1);
  lcd.print(" g");
  }
  if(berat>=1000)
  {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("SILAHKAN TIMBANG");
  float hasil=berat/1000;
  lcd.setCursor(0, 1);
  lcd.print("Berat= ");
  lcd.print(hasil);
  lcd.print(" kg");
  }
  scale.power_down();
  delay(2000);
  scale.power_up();
}
